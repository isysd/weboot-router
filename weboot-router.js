const COMPRE = /^component-(main|header|footer)$/
const ATTRRE = /^component-(main|header|footer)-.*$/

function getAttributesFromURL (target, url) {
  const lcompre = new RegExp(`^component-${target}-`)
  var attributes = {}
  var keys = url.searchParams.keys()
  for (var k of keys) {
    if (k.match(ATTRRE) && k.match(lcompre)) {
      attributes[k.replace(lcompre, '')] = url.searchParams.get(k)
    }
  }
  return attributes
}

async function loadComponent (target, comp, attributes) {
  var el = document.querySelector(target)
  if (el) {
    var html = `<${comp}`
    if (attributes && Object.keys(attributes).length > 0) {
      Object.keys(attributes).forEach(a => {
        html = `${html} ${a}="${attributes[a]}"`
      })
    }
    el.innerHTML = `${html}></${comp}>`
    var event = new Event('load')
    el.dispatchEvent(event)
    if (target === 'main') {
      if (history.state === null || ! history.state.hasOwnProperty('component') || (history.state.component && history.state.component !== comp)) {
        var search = `/${location.search}`
        var url = new window.URL(document.location.toString())
        if (search && search.length > 1) {
          if (url.searchParams.has('component-main') && url.searchParams.get('component-main') !== comp) {
            //search = search.replace(`component-main=${url.searchParams.get('component-main')}`, `component-main=${comp}`)
            search = `/?component-main=${comp}`
            if (attributes && Object.keys(attributes).length > 0) {
              Object.keys(attributes).forEach(a => {
                search += `&component-main-${a}=${attributes[a]}`
              })
            }
          }
        } else {
          search = `/?component-main=${comp}`
        }
        history.pushState({'component': comp, url: search}, comp, search);
      }
    }
  }
}

window.addEventListener('DOMContentLoaded', e => {
  var url = new window.URL(document.location.toString())
  var keys = url.searchParams.keys()
  var targets = []
  for (var k of keys) {
    if (k.match(COMPRE)) {
      targets.push(k)
    }
    // else if (k.match(ATTRRE))
  }
  if (targets && targets.length > 0) {
    targets.forEach(t => {
      loadComponent(t.split('-')[1], url.searchParams.get(t), getAttributesFromURL(t.split('-')[1], url))
    })
  } else if (history.state === null) {
    console.log(document.location.toString())
    history.replaceState({mainHTML: document.querySelector('main').innerHTML, url: window.location.toString()}, document.title, document.location.toString());
  }
})

window.onpopstate = function (e) {
  if (e.state && e.state.component) {
    if (e.state.url.startsWith('/') || e.state.url.startsWith('?')) e.state.url = `${location.origin}${e.state.url}`
    loadComponent('main', e.state.component, getAttributesFromURL('main', new URL(e.state.url)))
  } else if (e.state && e.state.mainHTML) {
    document.querySelector('main').innerHTML = e.state.mainHTML
  }
};

window.loadComponent = loadComponent
