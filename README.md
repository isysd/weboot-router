# weboot-router

Load webcomponents and weboots via global function or querystring. Serves as a basic router for navigating around apps.

### Usage

Set content for `header`, `footer`, and `main` by calling the global loadComponent function.

```
await loadComponent('main', 'component-to-display-in-main')
```

The same can be accomplished on `DOMContentLoaded` by using the following querystring: `?component-main=component-to-display-in-main`.

### License

MIT Copyright isysd

